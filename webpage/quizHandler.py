import sys
import json
import secrets
import logging
import boto3
import requests
from botocore.exceptions import ClientError


def main(args):
    send_data = {
        "quiz_slug" : "ecs_practice"
    }
    data = requests.post("https://pizg9b4aea.execute-api.us-east-1.amazonaws.com/default/new_token",json=send_data)
    token = json.loads(data)["token"]
    with open("./templates/index.html", "r") as html_file:
        newText = html_file.read().replace("$(tokenID)", token)
    with open("./templates/index.html", "w") as html_file:
        html_file.write(newText)

if __name__ == '__main__':
    main(sys.argv)